package me.nikx.dctower;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Models the central controlling server which accepts incoming {@link ElevationRequest ElevationRequests} and
 * distributes them on the registered elevators.
 * <p>
 * For more information about the algorithm used to distribute the requests see {@link #handleRequest(ElevationRequest)}.
 * <p>
 * New elevators can be added (registered) to this server by calling {@link #addElevator(char)} and can later be removed
 * again via {@link #removeElevator(char)}.
 * <p>
 * The controller runs in a separate thread, in real world this separate thread would most likely be a completely
 * different machine and communication would happen not via a queue but other (more physical) ways of transportation
 * (see {@link Display} for more information on this).
 */
public class TowerController implements Runnable {

    private static final Logger logger = LoggerFactory.getLogger(TowerController.class);

    private final BlockingQueue<ElevationRequest> elevationRequestQueue;
    private final ConcurrentHashMap<Character, Elevator> elevators;

    public TowerController(final BlockingQueue<ElevationRequest> elevationRequestQueue) {
        this.elevationRequestQueue = elevationRequestQueue;
        this.elevators = new ConcurrentHashMap<>();

        new Thread(this, "tower controller").start();
    }

    @Override
    public void run() {
        while (true) {
            try {
                final ElevationRequest request = this.elevationRequestQueue.take();

                if (request == ElevationRequest.POISON_PILL) {
                    logger.info("retrieved poison pill");
                    break;
                }

                /*
                 * Returning this information to the caller would require some form of Multiplexed Inter-Thread
                 * communication (like an HTTP Request -> Response). It could be implemented via Sockets for example,
                 * but this goes out of scope for this little simulation.
                 */
                final char fittingElevatorIdentifier = this.handleRequest(request);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        this.elevators.values().forEach(Elevator::deactivate);
    }

    /**
     * The algorithm performs the following steps and tries to find a fitting elevator:
     * <ol>
     *     <li>Try to find an elevator which is already moving in the right direction. This means two new stops need to
     *     be added to the current target set, one to pick the person up and one to let them out of the elevator
     *     again. The elevator will not need to change it's direction to bring this person to their target. Elevators
     *     which already have other target sets in their queue will not be used.</li>
     *     <li>Try to find a free (currently not moving) elevator which can be put into motion to pick the person up. A
     *     new target set needs to be added with two entries, one to pick the person up and one to let them out again.
     *     The elevator will not need to change it's direction to bring this person to their target.</li>
     *     <li>Try to find a free (currently not moving) elevator which can be first send to pick the person up and then
     *     to bring them to their target. This requires to two new target sets, the first one disallows the addition
     *     of new stops, the second does. The elevator will need to change it's direction for this to work (hence two
     *     target sets)</li>
     *     <li>At this point the elevator with the smallest target set queue will be used. This is the fallback.</li>
     * </ol>
     * <p>
     * The above algorithm could be expanded to make it more efficient, but I decided that this would be too much for
     * the given task.
     * <p>
     * Some things that could be done to improve the algorithm:
     * <ul>
     *     <li>The algorithm currently does not check any additional target sets for an elevator besides the current
     *     one. A clever algorithm would do this.</li>
     *     <li>Many small elevations (like two floors up) will all be routed to the same elevator which means that
     *     people at the top will have to wait very long until the elvator arrives there.</li>
     *     <li>It would be possible to count how many people are in the elevator and decide based on that if the
     *     elevator should accept additional requests.</li>
     * </ul>
     */
    private char handleRequest(ElevationRequest request) {
        final Elevator fittingElevator = this.tryUsingMovingElevator(request)
                .or(() -> tryUsingStoppedElevatorWithoutDirectionChange(request))
                .or(() -> tryUsingAnyStoppedElevator(request))
                .orElseGet(() -> tryUsingAnyElevator(request));

        logger.info("elevator '{}' will handle request '{}'", fittingElevator.identifier(), request);

        return fittingElevator.identifier();
    }

    private Optional<Elevator> tryUsingMovingElevator(ElevationRequest request) {
        logger.debug("trying to handle '{}' with an already moving elevator", request);

        final Optional<Elevator> elevator = this.elevators.values()
                .stream()
                .filter(el -> el.currentTargetSet() != null)
                .filter(el -> el.targetSetQueue().isEmpty())
                .filter(el -> el.currentTargetSet().direction() == request.direction())
                .filter(el -> el.currentTargetSet()
                        .direction()
                        .canFloorsBeReachedInOrder(el.currentFloor(), request.currentFloor()))
                .min(Comparator.comparingInt(el -> el.currentTargetSet().targetCount()));

        elevator.ifPresent(el -> {
            el.currentTargetSet().add(request.currentFloor());
            el.currentTargetSet().add(request.targetFloor());
        });

        return elevator;
    }

    private Optional<Elevator> tryUsingStoppedElevatorWithoutDirectionChange(ElevationRequest request) {
        logger.debug("trying to handle '{}' with a stopped elevator which do not need to change direction", request);

        final Optional<Elevator> elevator = this.elevators.values().stream()
                .filter(el -> el.currentTargetSet() == null)
                .filter(el -> request.direction().canFloorsBeReachedInOrder(el.currentFloor(), request.currentFloor()))
                .findAny();

        elevator.ifPresent(el -> {
            final TargetSet set = new TargetSet(request.direction(), request.currentFloor(), request.targetFloor());
            el.targetSetQueue().offer(set);
        });

        return elevator;
    }

    private Optional<Elevator> tryUsingAnyStoppedElevator(ElevationRequest request) {
        logger.debug("trying to handle '{}' with any stopped elevator", request);

        final Optional<Elevator> elevator = this.elevators.values().stream()
                .filter(el -> el.currentTargetSet() == null)
                .findAny();

        elevator.ifPresent(el -> {
            if (el.currentFloor() != request.currentFloor()) {
                final TargetSet first = new TargetSet(ElevationDirection.forFloors(el.currentFloor(), request.currentFloor()), request.currentFloor());
                el.targetSetQueue().offer(first);
            }

            final TargetSet second = new TargetSet(request.direction(), request.targetFloor());

            el.targetSetQueue().offer(second);
        });

        return elevator;
    }

    private Elevator tryUsingAnyElevator(ElevationRequest request) {
        logger.debug("trying to handle '{}' with any elevator", request);

        final Elevator elevator = this.elevators.values().stream()
                .min(Comparator.comparingInt(el -> el.targetSetQueue().size()))
                .orElseThrow(() -> new IllegalStateException("no elevators available"));

        final TargetSet set = new TargetSet(request.direction(), request.currentFloor(), request.targetFloor());
        elevator.targetSetQueue().offer(set);

        return elevator;
    }

    /*
     * Add and remove elevators to this tower (mainly needed for better testing capabilities).
     */

    public void addElevator(final char identifier) {
        logger.debug("adding and starting elevator '{}'", identifier);

        final Elevator elevator = new Elevator(identifier);

        new Thread(elevator, "elevator " + identifier).start();

        this.elevators.put(identifier, elevator);
    }

    public void removeElevator(final char identifier) {
        logger.debug("removing and stopping elevator '{}'", identifier);

        final Elevator elevator = this.elevators.remove(identifier);

        elevator.deactivate();
    }

    public void printStatus() {
        this.elevators.values().forEach(el -> {
            final List<TargetSet> sets = new java.util.ArrayList<>();
            sets.add(el.currentTargetSet());
            sets.addAll(el.targetSetQueue());

            final StringBuilder builder = new StringBuilder("Elevator '")
                    .append(el.identifier())
                    .append("' currently at floor ")
                    .append(el.currentFloor())
                    .append(":");

            for (final TargetSet set : sets) {
                builder.append("\n - ").append(set);
            }

            System.out.println(builder);
        });
    }
}
