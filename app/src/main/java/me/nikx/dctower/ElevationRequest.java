package me.nikx.dctower;

/**
 * Models a request to the {@link TowerController} for an elevator.
 */
public record ElevationRequest(int currentFloor, int targetFloor) {

    public static final ElevationRequest POISON_PILL = new ElevationRequest(Integer.MIN_VALUE, Integer.MAX_VALUE);

    public ElevationRequest {
        if (currentFloor == targetFloor) {
            throw new IllegalArgumentException("elevation request must be to a different floor");
        }
    }

    public ElevationDirection direction() {
        return ElevationDirection.forFloors(this.currentFloor, this.targetFloor);
    }

    @Override
    public String toString() {
        return "request(%s -> %s : %s)".formatted(this.currentFloor, this.targetFloor, this.direction());
    }
}
