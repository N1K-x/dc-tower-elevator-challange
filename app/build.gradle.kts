plugins {
    application
}

repositories {
    mavenCentral()
}

dependencies {
    implementation(group = "org.slf4j", name = "slf4j-api", version = "1.7.32")
    implementation(group = "ch.qos.logback", name = "logback-classic", version = "1.2.9")
}

application {
    mainClass.set("me.nikx.dctower.Main")
}
